"use strict"

/*  1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом.
Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.
 */

let firstNumber = prompt('Введіть перше число:');
while (firstNumber === null || firstNumber === '' || isNaN(firstNumber)) {
    firstNumber = prompt('Введіть перше число:');
}

let secondNumber = prompt('Введіть друге число:');
while (secondNumber === null || secondNumber === '' || isNaN(secondNumber)) {
    secondNumber = prompt('Введіть друге число:');
}

firstNumber = +firstNumber;
secondNumber = +secondNumber;

if (firstNumber <= secondNumber) {
    for (let i = firstNumber; i <= secondNumber; i++) {
        console.log(i);
    }
} else {
    for (let i = secondNumber; i <= firstNumber; i++) {
        console.log(i);
    }
}

/*
 Напишіть програму, яка запитує в користувача число та перевіряє,
 чи воно є парним числом. Якщо введене значення не є парним числом,
 то запитуйте число доки користувач не введе правильне значення.
 */

// let userNumber = prompt('Введіть парне число');
// while (userNumber === '' || userNumber % 2 !== 0) {
//     userNumber = prompt('Введіть число');
// }

